package generator.mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class MappingGenerator {	

	public static String generateMapping(File getters, String gettersObjectName, File setters, String settersObjectName) {
		
		if(getters.exists() && setters.exists()){
			return mapMethods(getMethods(getters, MethodType.GET), gettersObjectName, getMethods(setters, MethodType.SET), settersObjectName);		
		}
		
		return StringUtils.EMPTY;
	}
	
	private static String mapMethods(List<Method> getters, String gettersObjectName, List<Method> setters, String settersObjectName){
		StringBuilder builder = new StringBuilder();
		
		if(getters != null && setters != null){
			for(Method set : setters){
				Method get = getMethod(getters, set.getKey());
				if(get != null){
					builder.append(settersObjectName + "." + set.getMethodName() + "(" + gettersObjectName + "." + get.getMethodName() + ");\n");
				}
			}
		}
		
		return builder.toString();
	}
	
	private static Method getMethod(List<Method> list, String key){
		if(list != null && StringUtils.isNotEmpty(key)){
			for(Method method : list){
				if(StringUtils.equals(method.getKey(), key)){
					return method;
				}
			}
		}
		
		return null;
	}
	
	private static List<Method> getMethods(File file, MethodType methodType){
		List<Method> result = new ArrayList<>();
		
		if(file != null){
			try(BufferedReader br = new BufferedReader(new FileReader(file))){				
				String line;
				while((line = br.readLine()) != null){
					if(StringUtils.contains(line.toLowerCase(), methodType.getValue())){
						String methodName = StringUtils.substring(line, 
								StringUtils.indexOf(line, methodType.getValue()), 
								StringUtils.indexOf(line, "("));
						
						String key = StringUtils.remove(methodName, methodType.getValue());
						
						result.add(new Method(key, methodName));
					}
				}				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
		
		return result;
	}

}
