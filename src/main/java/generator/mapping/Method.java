package generator.mapping;

public class Method {

	private String key;
	private String methodName;
		
	public Method(String key, String methodName) {
		this.key = key;
		this.methodName = methodName;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}	
}
