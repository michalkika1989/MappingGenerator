package generator.mapping;

public enum MethodType {
	GET("get"), 
	SET("set");
	
	private String value;
	
	private MethodType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}	
}
